define([
    "dojo/_base/declare",
    "aps/_PopupView",
    "aps/load",
    "aps/ResourceStore",
    "./displayError",
    "aps/ready!"
], function (declare, _PopupView, load, Store, displayError) {

    var POPUP_PANEL = 'owc-bm-panel',
        POPUP_FIELDSET = 'owc-bm-FieldSet',
        PROFILES_COUNT = 'owc-bm-add-profiles-value',

        INITIAL_VALUE = 1;

    var loadPromise;

    return declare(_PopupView, {
        size: "sm",

        updateBuyAmount: function(newValue) {
            if (isNaN(newValue) || (+newValue <= 0) || !this.owcProfile) return;
            return aps.biz.requestLimitsChange({
                limits: [{
                    apsId: this.owcProfile.apsId,
                    limit: +newValue + this.owcProfile.limit
                }]
            })
            .then(null, displayError);
        },

        init: function () {
            loadPromise = load([
                "aps/Panel", {id: this.genId(POPUP_PANEL)}, [
                    ["aps/FieldSet", {id: this.genId(POPUP_FIELDSET)}, [
                        ["aps/Spinner", {
                            id: this.genId(PROFILES_COUNT),
                            gridSize: 'xs-12 md-4',
                            label: _('Additional Profiles Quantity'),
                            required: true,
                            minimum: INITIAL_VALUE,
                            maximum: 99,
                            value: INITIAL_VALUE
                        }]
                    ]]
                ]
            ], this.domNode);

            return loadPromise.then(function () {
                var widget = this.byId(PROFILES_COUNT);
                widget.watch('value', function (name, oldValue, newValue) {
                    this.updateBuyAmount(newValue);
                }.bind(this));
            }.bind(this));
        },
        onContext: function () {
            var profile = aps.context.vars.profile;

            return loadPromise.then(function () {
                var popupTitle = _('Buy Additional "__name__ __quota__ GB" Profiles', {name: profile.name, quota: profile.quota});
                this.set("label", popupTitle);
                aps.apsc.buttonState("submit", {disabled: true});

                return aps.biz.getResourcesInfo({filter: [{apsId: profile.aps.id}]});
            }.bind(this))
            .then(function (resources) {
                if (resources.length == 1) {
                    var profileInfo = resources[0],
                        spinner = this.byId(PROFILES_COUNT);

                    this.owcProfile = profileInfo;

                    spinner.set({
                        maximum: (profileInfo.max >= 0) ? (profileInfo.max - profileInfo.limit) : null,
                        description: profileInfo.priceText,
                        value: INITIAL_VALUE
                    });
                    spinner.focus();
                    return this.updateBuyAmount(1);
                }
            }.bind(this))
            .then(function () {
                aps.apsc.buttonState("submit", {disabled: false});
                aps.apsc.hideLoading();
            }, displayError);
        },
        onCancel: function () {
            this.cancel();
        },
        onSubmit: function() {
            if (!this.byId(PROFILES_COUNT).validate()) {
                aps.apsc.cancelProcessing();
            } else {
                var amount = this.byId(PROFILES_COUNT).get('value') + this.owcProfile.limit,
                    profile = aps.context.vars.profile;

                aps.biz.requestLimitsChange({
                    limits: [{
                        apsId: profile.aps.id,
                        limit: amount
                    }]
                })
                .then(function() {
                    return aps.biz.commit();
                })
                .then(function() {
                    this.submit();
                }.bind(this), displayError);
            }
        },
        onHide: function() {
            this.owcProfile = undefined;
            this.byId(PROFILES_COUNT).set({minimum: INITIAL_VALUE, maximum: 99, value: INITIAL_VALUE});
            aps.apsc.buttonState("submit", {disabled: true});
        }
    });
});
