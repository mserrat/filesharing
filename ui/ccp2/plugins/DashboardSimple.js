define([
    "dojo/_base/declare",
    "aps/common",
    "aps/i18n",
    "aps/nav/ViewPlugin",
    "module",
    "../../common/TYPES"
], function (declare, common, i18n, ViewPlugin, module, TYPES)
{
    var _ = i18n.bindToPackage(common.getPackageId(module));

    return declare(ViewPlugin, {
        apsType: TYPES.SAMPLES_TYPE,
        resourceName: _('Profiles'),
        totalSubtitle: _('Assigned to Users'),
        entryViewId: 'service',
        promoPrimaryText: _('Save Your Files in Cloud')
    });
});

