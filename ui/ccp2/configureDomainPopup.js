define([
    "dojo/_base/declare",
    "./assignDomainPopup"
], function (declare, AssignDomainPopup) {
    "use strict";
    return declare(AssignDomainPopup);
});
