define([
    "dojo/_base/declare",
    "dojox/mvc/getStateful",
    "aps/_View",
    "aps/ResourceStore",
    "aps/xhr",
    "aps/tiles/UsageInfoTile",
    "./displayError",
    "../common/TYPES"
], function (declare, getStateful, _View, Store, xhr, UsageInfoTile, displayError, TYPES) {

    var PROFILES_LIST = 'owc-sign-profiles-list',
        PROFILE_TILE = 'owc-sign-profile-',
        USER_NAME = 'owc-sign-username',
        USER_PWD = 'owc-sign-password',
        PWD_MIN = 7,
        PWD_MAX = 64;

    var SIGNUP_VIEW = 'http://www.parallels.com/ccp-signup#signupview';

    var profileValue;

    return declare(_View, {
        init: function () {
            var self = this;

            return [
                ["aps/Output", {id: this.genId("owc-sign-cfg-out"), content: _('Configure File Sharing service.')}],
                ["aps/Tiles", {
                    id: this.genId(PROFILES_LIST),
                    title: _('Select Your File Sharing Profile'),
                    selectionMode: 'single',
                    required: true,
                    onClick: function () {
                        var selectedTile = self.byId(this.get('selectionArray')[0]);
                        profileValue = selectedTile.owcProfileId;
                    }
                }],
                ["aps/Container", {
                    id: this.genId("owc-sign-username-group"),
                    title: _('Setup Your File Sharing User')
                }, [
                    ["aps/Panel", {
                        id: this.genId("owc-sign-username-panel")
                    }, [
                        ["aps/FieldSet", {
                            id: this.genId("owc-sign-username-container")
                        }, [
                            ["aps/Output", {
                                id: this.genId(USER_NAME),
                                label: _("Username")
                            }],
                            ["aps/Password", {
                                id: this.genId(USER_PWD),
                                gridSize: 'xs-6 md-3',
                                label: _("Password"),
                                showStrengthIndicator: true,
                                required: true,
                                customValidator: function (value) {
                                    if (value === null || value.length < PWD_MIN || value.length > PWD_MAX) return false;
                                    return true;
                                }
                            }]
                        ]]
                    ]]
                ]]
            ];
        },
        onContext: function () {
            function addProfiles() {
                var wizardData=aps.context.wizardData[SIGNUP_VIEW],
                    profilesList = this.byId(PROFILES_LIST),
                    selectedId,
                    selectedProfile,
                    store = new Store({
                        target: "/aps/2/resources/",
                        apsType: TYPES.SAMPLES_TYPE
                    });

                profilesList.removeAll();

                return store.query("sort(-quota)")
                .then(function (samples) {
                    samples.forEach(function (nextSample) {
                        var nextRate = wizardData.planRates.find(function(rate) {
                             return rate.apsType === TYPES.SAMPLES_TYPE &&
                                    rate.apsId == nextSample.aps.id &&
                                    rate.included > 0;
                        });
                        if (!nextRate) return;

                        var nextButtonId = PROFILE_TILE + nextRate.apsId;
                        var nextButton = this.byId(nextButtonId);
                        if (nextButton) nextButton.destroy();

                        var widgetId = this.genId(nextButtonId);
                        nextButton = new UsageInfoTile({
                            id: widgetId,
                            title: nextRate.title,
                            owcProfileId: nextRate.apsId,
                            value: nextSample.quota,
                            textSuffix: _('GB Storage'),
                            usageHint: (
                                nextRate.included > 1 ?
                                    _('__included__ Profiles Available', {included: nextRate.included})
                                    : _('__included__ Profile Available', {included: nextRate.included})
                            ),
                            showPie: false
                        });
                        profilesList.addChild(nextButton);

                        if (!selectedId || nextRate.apsId == profileValue) {
                              // first added profile will be selected if there
                              // is no selected profile
                            selectedProfile = nextRate.apsId;
                            selectedId = widgetId;
                        }
                    }, this);

                    if (selectedId) {
                        profilesList.set('selectionArray', getStateful([selectedId]));
                        profileValue = selectedProfile;
                    }

                    var widget = this.byId(USER_NAME);
                    if (wizardData.objects.length > 0 && 'users' in wizardData.objects[0] && wizardData.objects[0].users.length > 0) {
                        widget.set('value', wizardData.objects[0].users[0].login);
                    }

                }.bind(this));
            }

            return addProfiles.call(this).then(aps.apsc.hideLoading, displayError);
        },
        onPrev: function () {
            aps.apsc.prev();
        },
        onNext: function () {
            var pwdField = this.byId(USER_PWD);
            if (!pwdField.validate()) {
                aps.apsc.cancelProcessing();
                return;
            }

            if (!profileValue) {
                aps.apsc.next();
            } else {
                var data = {
                    aps: {
                        type: TYPES.USERS_TYPE
                    },
                    samples:{
                        aps: {
                            id: profileValue
                        }
                    },
                    owncloudpassword: pwdField.get('value')
                };
                aps.apsc.next(data);
            }
        }
    });
});
