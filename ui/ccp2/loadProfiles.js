define([
    "aps/ResourceStore",
    "dojo/Deferred",
    "dojo/promise/all",
    "dojox/mvc/getStateful",
    "aps/xhr",
    "./displayError",
    "../common/TYPES"
], function (Store, Deferred, all, getStateful, xhr, displayError, TYPES) {

    return {
        getBillingSubscription: function(tenantObj, billingObj) { // FIXME remove obsolete non-use
            var def = new Deferred();

            if (tenantObj.billing !== true && tenantObj.billing !== 1){
                def.resolve(null);
            }else{
                xhr.get('/aps/2/resources/' + billingObj.aps.id + '/sm/subscriptions?subscriptionId=' + tenantObj.TENANTID).then(function (billingSubscription) {
                    if (billingSubscription.length > 0){
                        def.resolve(billingSubscription[0]);
                    }else{
                        def.resolve(null);
                    }
                });
            }
            return def;
        },

        load: function (tenantObj, options) { // FIXME remove obsolete non-use
            var def = new Deferred();

            if (options && options.oldList && !options.billing && !options.subscription){
                if (options.memory){
                    for (var i = 0; i < options.oldList.length; ++i) {
                        options.memory.put(options.oldList[i]);
                    }
                }
                def.resolve(options.oldList);
                return def;
            }

            var store = new Store({
                    target: "/aps/2/resources/"
                }),

                profilesList = [],
                billingInfo,

                queryList = [
                    store.get(tenantObj.aps.subscription + "/resources"),
                    store.query("implementing(" + TYPES.SAMPLES_TYPE + "),sort(-quota)")
                ],
                needBillingInfo = false;

            if (options && options.billing){
                queryList.push(this.getBillingSubscription(tenantObj, options.billing));
                needBillingInfo = true;
            }

            if (options && options.subscription){
                billingInfo = options.subscription.resourceRates;
            }
            all(queryList).then(function(results) {
                var resources = results[0],
                    samples = results[1];

                if(needBillingInfo && results[2] && 'resourceRates' in results[2]) billingInfo = results[2].resourceRates;

                for (var l=0; l < samples.length; l++) {
                    for (var i = 0; i < resources.length; i++) {
                        if (resources[i].apsType === TYPES.SAMPLES_TYPE &&
                            resources[i].apsId === samples[l].aps.id)
                        {
                            var shallBeChecked = false;

                            samples[l].limit = resources[i].limit;
                            samples[l].paresource = resources[i].id;
                            if (resources[i].limit!==0){
                                if(aps.app.model.visibleProfiles === undefined) {
                                    aps.app.model.visibleProfiles = 1;
                                } else {
                                    aps.app.model.visibleProfiles = aps.app.model.visibleProfiles + 1;
                                }
                            }
                            samples[l].usage = resources[i].usage;
                            if (samples[l].limit > samples[l].usage) {
                                samples[l].disabled = false;
                                if (shallBeChecked === false) {
                                    shallBeChecked = true;
                                    samples[l].checked = true;
                                } else {
                                    samples[l].checked = false;
                                }
                            } else {
                                samples[l].checked = false;
                                samples[l].disabled = true;
                            }

                            if (billingInfo){
                                for(var m = 0; m < billingInfo.length; m++){
                                    if (samples[l].paresource == billingInfo[m].resourceId){
                                        samples[l].billingInfo = billingInfo[m];
                                        break;
                                    }
                                }
                            }

                            if (options && options.memory) {
                                options.memory.put(samples[l]);
                            }
                            profilesList[profilesList.length] = samples[l];
                            break;
                        }
                    }
                }
                def.resolve(profilesList);
            }, function (err) {
                def.reject(err);
            });
            return def;
        },
        getProfilesAmount: function(tenantObj, options) {
            var memory,
                store = new Store({
                    target: "/aps/2/resources/"
                });

            options = options || {};

            if (options.memory) {
                memory = options.memory;
            }

            var reqMap = {resources: aps.biz.getResourcesInfo({filter: [{apsType: TYPES.SAMPLES_TYPE}]})};
            if (!options.noSamples) reqMap['samples'] = store.query("implementing(" + TYPES.SAMPLES_TYPE + "),sort(-quota)");

            return all(reqMap).then(function (data) {
                if (!options.canUseMore && !options.canBuyMore) {
                    return data;
                } else {
                    return all(data.resources.reduce(function (results, profile) {
                        results[profile.apsId] = aps.biz.canChangeUsage({
                            deltas: [{
                                apsType: profile.apsType,
                                apsId: profile.apsId,
                                delta: options.canUseMore ? options.canUseMore : (profile.limit - profile.usage + options.canBuyMore)
                            }]
                        });
                        return results;
                    }, data));
                }
            })
            .then(function (data) {
                var resources = data.resources,
                    samples = data.samples;
                delete data['resources'];
                delete data['samples'];

                if (options.noSamples) {
                    if (data) {
                        return resources.map(function (profile) {
                            if (data[profile.apsId]) profile._canMore = (options.canUseMore ? options.canUseMore : options.canBuyMore);
                            return profile;
                        });
                    } else {
                        return resources;
                    }
                } else {
                    return samples.map(function (sample) {
                        var resource = resources.find(function(resource) { return resource.apsId == sample.aps.id; });
                        if (resource) {
                            resource._sample = sample;
                            if (data && data[resource.apsId]) resource._canMore = (options.canUseMore ? options.canUseMore : options.canBuyMore);
                            if (memory) {
                                memory.put(getStateful(resource));
                            }
                            return resource;
                        }
                        return sample;
                    });
                }
            })
            .then(null, displayError);
        }
    };
});
