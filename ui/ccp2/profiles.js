define([
    "dojo/_base/declare",
    "aps/_View",
    "aps/Gauge",
    "aps/Tile",
    "./loadProfiles",
    "./displayError",
    "aps/ready!"
], function(declare, _View, Gauge, Tile, loadProfiles, displayError) {

    var PROFILES_TILES = 'owc-pl-profilestiles',
        PROFILE_TILE_PREFIX = 'owc-pl-profile-tile-',
        PROFILE_INFO_PREFIX = 'owc-pl-profile-info-',

        BUY_PROFILES_POPUP = 'http://www.parallels.com/samples/sample-aps2-owncloud-ldap-nextcp#buymoreprofiles';

    return declare(_View, {
        updateProfile: function (profile) {
            var visibilitywidget = true,
                titleText = _('__name__ (__quota__ GB)', {name: profile._sample.name, quota: profile._sample.quota});

            if (profile.limit === 0 && !profile._canMore) {
                visibilitywidget = false;
            }

            var profileTile = this.byId(PROFILE_TILE_PREFIX + profile.apsId),
                profileInfo = this.byId(PROFILE_INFO_PREFIX + profile.apsId);

            if (!profileTile) {
                profileTile = new Tile({
                    visible: visibilitywidget,
                    title: titleText,
                    id: this.genId(PROFILE_TILE_PREFIX + profile.apsId),
                    gridSize: 'xs-12 md-6'
                });
                this.byId(PROFILES_TILES).addChild(profileTile);
            }
            if (!profileInfo) {
                profileInfo = new Gauge({
                    id: this.genId(PROFILE_INFO_PREFIX + profile.apsId),
                    minimum: 0,
                    legend: _('${value} of ${maximum} assigned to users'),
                    noneText: _('Not purchased yet'),
                    unlimitedText: _('${value} assigned to users (Unlimited)'),
                    classesMap: {}
                });
                profileTile.addChild(profileInfo);
            }

            profileInfo.set({
                maximum: (profile.min == -1) ? undefined : profile.limit, // XXX .min APS-36940
                value: profile.usage
            });

            if (!profile._canMore || profile.min == -1 || profile.limit === undefined) { // XXX .min APS-36940
                profileTile.set('buttons', []);
            } else {
                var buyMoreFunc = function () {
                    this.nav.showPopup({
                        viewId: BUY_PROFILES_POPUP,
                        resourceId: profile.apsId,
                        modal: false
                    });
                }.bind(this);
                var notPurchased = (profile.limit === 0 && profile.usage === 0);
                profileTile.set({
                    buttons: [
                        {
                            'title': notPurchased ? _('Buy') : _('Buy Additional Profiles'),
                            iconClass: "fa-plus",
                            autoBusy: false,
                            onClick: buyMoreFunc
                        }
                    ]
                });
            }
        },

        init: function () {
            return ["aps/Container", [
                ["aps/Tiles", {id: this.genId(PROFILES_TILES)}]
            ]];

        },
        onContext: function () {

            loadProfiles.getProfilesAmount(aps.context.vars.tenant, {canBuyMore: 1})
            .then(function (profiles) {
                profiles.forEach(this.updateProfile, this);
                aps.apsc.hideLoading();
            }.bind(this), displayError);
        },
        onHide: function() {
        }
    });
});
