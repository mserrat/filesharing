define([
    "dojox/mvc/getStateful",
    "aps/Tile",
    "aps/tiles/UsageInfoTile"
], function (getStateful, Tile, UsageInfoTile) {

    return {
        addProfiles: function(self, profilesList, tilePrefix, profiles, options) {
            var profilesCount = 0,
                disabledProfiles = 0,
                selectedId,
                profileNeedUpsell = false;

            options = options || {};
            profilesList.removeAll();

            profiles.forEach(function (nextProfile) {
                var needUpsell = false;

                if (!nextProfile || (nextProfile.max === 0 && nextProfile.usage === 0)) return;

                ++profilesCount;

                var availableText,
                    noAvailable = false,
                    existingAmount = 0;

                if (options.used && options.used[nextProfile.apsId]) existingAmount = options.used[nextProfile.apsId];

                if (nextProfile.min === -1 || nextProfile.limit === undefined) { // XXX .min APS-36940
                    availableText = _('Unlimited Available', self);
                } else {
                    if ((nextProfile.usage - existingAmount) < nextProfile.limit) {
                        availableText = _('__available__ Profiles Available', {available: (nextProfile.limit - nextProfile.usage + existingAmount)}, {}, self);
                    } else {
                        availableText = _('No Profiles Available', self);
                        noAvailable = true;
                    }
                }

                var imDisabled = false,
                    dueAmount = false,
                    amountDue = 0,
                    priceString = '',
                    additionalProfiles = 1;

                if (options.additional) additionalProfiles = options.additional;

                if (nextProfile.min !== -1 && nextProfile.limit !== undefined) { // XXX .min APS-36940
                    var newAmount = nextProfile.usage + additionalProfiles - existingAmount;
                    if (newAmount > nextProfile.limit) {
                        if (options.upsellDisabled ||
                            !nextProfile._canMore || nextProfile._canMore < additionalProfiles)
                        {
                            imDisabled = true;
                            dueAmount = true;
                        } else {
                            amountDue = newAmount - nextProfile.limit;
                            priceString = _(
                                '__priceText__ for __amount__ Additional Profiles', // XXX text
                                {amount: amountDue, priceText: nextProfile.priceText},
                                {}, self
                            );
                            needUpsell = true;
                        }
                    }
                }

                if (imDisabled) ++disabledProfiles;
                nextProfile.amountDue = amountDue;
                nextProfile.amountTotal = additionalProfiles;

                if (dueAmount === true && !noAvailable) {
                    availableText = _('__available__ / __comment__', {available: availableText, comment: _("can not be used for assigning it to the __count__ new users", {count: additionalProfiles})}, {}, self);
                }

                var nextButtonId = self.genId(tilePrefix + nextProfile.apsId),
                    imChecked = false;

                if (!imDisabled && (!selectedId || (options.selectedId && nextProfile.apsId == options.selectedId))) {
                    imChecked = true;
                    selectedId = nextButtonId;
                    profileNeedUpsell = needUpsell;
                }
                nextProfile.needUpsell = needUpsell;
                var nextButton = new UsageInfoTile({
                    id: nextButtonId,
                    title: nextProfile._sample.name,
                    _profile: nextProfile,
                    gridSize: 'md-4 xs-12',
                    disabled: imDisabled,
                    value: nextProfile._sample.quota,
                    textSuffix: _('GB Storage', self),
                    description: availableText,
                    usageHint: priceString,
                    showPie: false
                });
                profilesList.addChild(nextButton);
            }, this);

            if (profilesCount === disabledProfiles) {
                var noneId = self.genId(tilePrefix + 'none');
                profilesList.addChild(new Tile({
                    id: noneId,
                    _profile: undefined,
                    title: _('None', self),
                    gridSize: 'md-4 xs-12',
                    disabled: false
                }));
                selectedId = noneId;
                profileNeedUpsell = false;
            }
            profilesList.set('selectionArray', getStateful([selectedId]));

            return profileNeedUpsell;
        }
    };
});
