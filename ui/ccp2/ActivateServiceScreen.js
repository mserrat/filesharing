define([
    "dojo/_base/declare",
    "aps/_View"
], function(declare, _View) {

    var USERNAME = "owc-activate-userName",
        MY_USERNAME_PANEL = "owc-activate-userName-panel",
        MY_PROFILE_PANEL = "owc-activate-profile-panel",
        MY_PROFILE = "owc-activate-profile",
        PWD_FIELD  = "owc-activate-password",
        PWD_MIN = 7,
        PWD_MAX = 64;

    var userData = {user:{}};

    return declare(_View, {
        init: function() {
            return [
                ["aps/Panel", { id: this.genId(MY_PROFILE_PANEL), title: _("My File Sharing Profile")}, [
                   ["aps/Output", {
                       id: this.genId(MY_PROFILE),
                       label: _('My Sharing Profile')
                   }]
                ]],
                ["aps/Panel", { id: this.genId(MY_USERNAME_PANEL), title: _("My File Sharing Login")}, [
                    ["aps/FieldSet", {
                        gridSize: "md-12 xs-12"
                    },[
                        ["aps/Output", {
                            id: this.genId(USERNAME),
                            label: _('Username')
                        }],
                        ["aps/Password",{
                            id: this.genId(PWD_FIELD),
                            label: _('Password'),
                            showStrengthIndicator: true,
                            required: true,
                            gridSize: "md-3 xs-12",
                            customValidator: function(value) {
                                if (value === null || value.length < PWD_MIN || value.length > PWD_MAX) return false;
                                return true;
                            }
                        }]
                    ]]
                ]]
            ];
        },

        onContext: function () {
            function drawLogin(ownCloudUserLogin){
                userData.id = ownCloudUserLogin.aps.id;
                this.byId(MY_PROFILE).set(
                    "value",
                    _('__name__ (__quota__ GB)', {
                        name: ownCloudUserLogin.assignedprofilename,
                        quota: ownCloudUserLogin.quota
                    })
                );
                this.byId(USERNAME).set("value", ownCloudUserLogin.owncloudusername);
            }

            function updateData() {
                var userObj = aps.context.vars.endusers.find(
                    function (endUser) {return endUser.user.userId === aps.context.user.userId;}
                );
                if (userObj) drawLogin.call(this, userObj);
            }

            updateData.call(this);
            aps.apsc.hideLoading();
        },

        onNext: function () {
            var pwdField = this.byId(PWD_FIELD);
            if(!pwdField.validate()){
                aps.apsc.cancelProcessing();
                return;
            }
            userData.user.owncloudpassword = pwdField.get('value');
            var savedXhr = {
                url: '/aps/2/resources/' + userData.id + '/changepassword',
                method: 'PUT',
                headers: {
                    "Content-Type": 'application/json'
                },
                data: JSON.stringify(userData.user)
            };
            aps.apsc.next({objects: [{xhr: savedXhr}]});
        }

    });// Close of return declare
}); // Close of Main function
