define([
    "dojo/_base/declare",
    "aps/_View",
    "./loadProfiles",
    "./profilesTools",
    "./displayError",
    "../common/TYPES",
    "dojo/text!../json/ownclouduser.json"
], function (
    declare, _View,
    loadProfiles, profilesTools, displayError,
    TYPES, ownCloudUserJson
) {
    var DESCRIPTION = 'owc-au-description',
        PROFILES_LIST = 'owc-au-profiles-list',
        PROFILE_TILE = 'owc-au-profile-';

    function updateResourcesAmount(operations) {
        var tilesList = this.byId(PROFILES_LIST);
        var selection = tilesList.get('selectionArray'),
            tiles = tilesList.getChildren(),
            deltas = [],
            selectedTileId;

        if (selection.length > 0) selectedTileId = selection[0];

        tiles.forEach(function (tile) {
            if (tile.get('disabled')) return;
            var profile = tile._profile;
            if (!profile) return;
            deltas.push({
                apsId: profile.apsId,
                delta: ((tile.id === selectedTileId) ? profile.amountTotal : 0)
            });
        });

        var params = {'deltas': deltas};
        if (operations) params.operations = operations;
        return aps.biz.requestUsageChange(params);
    }

    return declare(_View, {
        init: function () {
            return [
                ["aps/Output", {
                    id: this.genId(DESCRIPTION),
                    content: _('Select which File Sharing profile you want to assign to the new users.'),
                    'class': 'lead'
                }],
                ["aps/Tiles", {
                    id: this.genId(PROFILES_LIST),
                    title: _('Select Profile'),
                    selectionMode: 'single',
                    required: true,
                    onClick: updateResourcesAmount.bind(this, null)
                }]
            ];
        },
        onContext: function() {
            var usersCount = 0;

            return aps.biz.getResourcesToBind()
            .then(function (data) {
                usersCount = data.users.length;
                return loadProfiles.getProfilesAmount(aps.context.vars.tenant, {canUseMore: usersCount});
            })
            .then(function(profiles) {
                profilesTools.addProfiles(
                    this, this.byId(PROFILES_LIST), PROFILE_TILE, profiles,
                    {additional: usersCount}
                );
                return updateResourcesAmount.call(this, null);
            }.bind(this))
            .otherwise(displayError)
            .always(aps.apsc.hideLoading);
        },
        onPrev: function() {
            aps.apsc.prev();
        },
        onNext: function() {
            var profileValue,
                tenant = aps.context.vars.tenant,
                selection = this.byId(PROFILES_LIST).get('selectionArray');

            if (selection.length > 0) {
                var tile = this.byId(selection[0]);
                if (tile && tile._profile) profileValue = tile._profile.apsId;
                if (!profileValue) {
                    aps.apsc.next();
                    return;
                }
            }

            aps.biz.getResourcesToBind().then(function(toBind) {
                var operations = toBind.users.map(function (user) {
                    var owcUser = JSON.parse(ownCloudUserJson);
                    owcUser.aps.subscription = tenant.aps.subscription;
                    owcUser.samples.aps.id = profileValue;
                    owcUser.tenant.aps.id = tenant.aps.id;
                    owcUser.user.aps.id = user.aps.id;
                    return {provision: owcUser};
                });
                return operations.length ? updateResourcesAmount.call(this, operations) : null;
            }.bind(this))
            .then(function() {aps.apsc.next();}, displayError);
        }
    });
});
