define([
    "dojo/_base/declare",
    "./ChangePassword"
], function (declare, ChangePasswordPopup) {
    "use strict";
    return declare(ChangePasswordPopup);
});
