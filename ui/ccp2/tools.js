define([
    "dijit/registry"
], function (registry) {
    return {
        getWidget: function (Widget, widgetId, params, checkType) {
            var  widget;

            if (!widgetId) return widget;
            widget = registry.byId(widgetId);

            if (widget && checkType && (widget instanceof Widget)) {
                widget.destroy();
                widget = undefined;
            }
            widget = widget || new Widget({id: widgetId});
            if (params) widget.set(params);

            return widget;
        }
    };
});
