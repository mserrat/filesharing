require([
    "aps/ResourceStore",
    "dojo/when",
    "dijit/registry",
    "aps/load",
    "aps/Memory",
    "dojox/mvc/getStateful",
    "dojox/mvc/getPlainValue",
    "aps/WizardData",
    "dojox/mvc/at",
    "aps/Message",
    "dojo/text!../json/ownclouduser.json",
    "aps/ready!"
], function (Store, when, registry, load, Memory, getStateful, getPlainValue, wizardData, at, Message, ownclouduser) {
        ownclouduser = JSON.parse(ownclouduser);
        var model =getStateful(wizardData.get());
        var wizards = [
        { text: _("Select User") },
        { text: _("Confirm Settings"), active: true }
        ];
        load(["aps/PageContainer", { id: "pageContainer" }, [
            ["aps/WizardControl", { steps: wizards }],
            ["aps/FieldSet", { title:_('Selected User') }, [
            ["aps/Output", { label: _('Name'), value: at(model.userModel,'fullName') }],
            ["aps/Output", { label: _('Email'), value: at(model.userModel,'email') }],
            ["aps/Output", { label: _('Login'), value: at(model.userModel,'login') }]
            ]],
            ["aps/FieldSet", { title: _('Selected Profile') }, [
            ["aps/Output", { label: _('Profile Type'), value: at(model.profilesModel,'name') }],
            ["aps/Output", { label: _('Storage assigned in GB'),value: at(model.profilesModel,'quota') }]
            ]]
            ]]).then(function(){
                aps.app.onCancel = function() {
                    aps.apsc.gotoView("ccpusers");
                };
                aps.app.onSubmit = function(){
                    ownclouduser.samples.aps.id = model.profilesModel.aps.id;
                    ownclouduser.user.aps.id = model.userModel.aps.id;
                    ownclouduser.tenant.aps.id = aps.context.vars.tenant.aps.id;
                    ownclouduser.owncloudpassword = model.userModel.owncloudpassword;
                    var tenantStore = new Store({
                        target:        "/aps/2/resources/" + aps.context.vars.tenant.aps.id + "/users"
                    });
                    when(tenantStore.put(ownclouduser), function() {
                      aps.apsc.gotoView("ccpusers");
                  }, function(err) {
                      var page = registry.byId("pageContainer"),
                      messages = page.get("messageList"),
                      errData = {};

                      errData = JSON.parse(err.response.text);
                      aps.apsc.cancelProcessing();
                      messages.removeAll();
                      messages.addChild(new Message({description: err + (errData.message ? "<br />" + errData.message : ""), type: "error"}));
                  });
                };
            });
});
