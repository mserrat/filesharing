require([
    "aps/load",
    "aps/Gauge",
    "./loadProfiles.js",
    "dojo/when",
    "aps/Store",
    "aps/Message",
    "dojox/mvc/getStateful",
    "dojox/mvc/at",
    "aps/ResourceStore",
    "aps/xhr",
    "dijit/registry",
    "../common/TYPES.js",
    "dojo/promise/all",
    "aps/Memory",
    "aps/ready!"
], function (load, Gauge, loadProfiles, when, Store, Message, getStateful, at, ResourceStore, xhr, registry, TYPES, all, Memory) {
    var owcUsersStore = new Store({
        apsType: TYPES.USERS_TYPE,
        target: "/aps/2/resources/" + aps.context.vars.tenant.aps.id + "/users"
    }),
    usersStore = new Store({
        target: "/aps/2/resources"
    }),
    generalStore = new Store({
        target: "/aps/2/resources"
    }),
    profilesMemory = new Memory({
                idProperty: "aps.id"
            });
    when(generalStore.get(aps.context.vars.tenant.aps.id), function(theTenantOnContext){
        all([
            owcUsersStore.query(),
            usersStore.query('implementing(http://aps-standard.org/types/core/user/1.0)'),
            loadProfiles(theTenantOnContext.subscription.aps.id, profilesMemory, TYPES)
            ]).then(function(results){
                var owcUsers = results[0],
                    serviceUsers = results[1],
                    usersMemory = new Memory({
                    idProperty: "aps.id"
                });
                for (var i = 0; i < owcUsers.length; i++){
                    var temp = getStateful(owcUsers[i]);
                    for (var j = 0; j < serviceUsers.length; j++){
                        if(serviceUsers[j].aps.id === owcUsers[i].serviceUserId){
                            temp.set({
                                displayName: serviceUsers[j].displayName
                            });
                        }
                    }
                    usersMemory.add(temp);
                }
                load(["aps/PageContainer", { id: "page" }, [
                    ["aps/Grid", {
                        id: "grid",
                        store: usersMemory,
                        selectionMode: 'multiple',
                        apsResourceViewId: "users-view-edit",
                        columns: [
                            { field: 'displayName', name: _('Display Name'), type: 'resourceName', filter: { title: _('Display Name')} },
                            { field: 'owncloudusername', name: _('Login'), filter: { title: _('Login')}},
                            { field: 'aps.status', name: _('Status'), renderCell: function(object){
                                var mystatus = object.aps.status;
                                if( mystatus === "aps:proto"){
                                    return " ";
                                }
                                else if( mystatus === "aps:provisioning"){
                                    return _('Assigning');
                                }
                                else if ( mystatus === "aps:ready"){
                                    return _('Ready');
                                }
                                else if ( mystatus === "aps:unprovisioning"){
                                    return _('Unprovisioning');
                                }
                                else if ( mystatus === 'none'){
                                    return _('Not Assigned');
                                }
                                else if ( mystatus === 'aps:activating'){
                                    return _('Not Activated');
                                }
                                else{
                                    return _('Unknown');
                                }
                            } },
                            { field: 'assignedprofilename', name: _('Profile') },
                            { field: 'userusage', name: _('Storage Usage'), renderCell: function(object){
                                return new Gauge({
                                    minimum: 0,
                                    maximum: object.quota,
                                    value: object.userusage,
                                    legend: "${value} GB " + _('used of ') + "${maximum} GB"
                                });
                            }}
                        ]},
                        [
                            ["aps/Toolbar", [
                                ["aps/ToolbarButton", { id: "newuser", iconName: "/pem/images/icons/new_16x16.gif", label: _('Add Existing Users') }],
                                ["aps/ToolbarButton", { id: "deleteuser", iconName: "/pem/images/icons/delete_16x16.gif", label: _('Delete'), requireItems: true }]
                            ]]
                        ]
                    ]
                ]]).then(function(){
                    var page = registry.byId("page"),
                        messages = page.get("messageList"),
                        grid = registry.byId("grid");
                    registry.byId("deleteuser").on("click", function() {
                        var sel = grid.get("selectionArray"),
                            count = 0,
                            self = this;
                        for(var i=0; i < sel.length; i++){
                            var apsIdSelected = sel[i];
                            when(generalStore.remove(apsIdSelected), function(){
                                when(usersMemory.remove(apsIdSelected), function(){
                                    sel.splice(i,1);
                                    if(++count >= sel.length){
                                        grid.refresh();
                                        self.cancel();
                                    }
                                });
                            }, function(err){
                                messages.removeAll();
                                messages.addChild(new Message({ description: err, type: "error" }));
                                if(++count >= sel.length){
                                    grid.refresh();
                                    self.cancel();
                                }
                            });
                        }
                    });
                    registry.byId("newuser").on("click", function(){
                        var self = this;
                        when(profilesMemory.query("limit=ne=0"), function(resultsProfiles){
                            if(resultsProfiles.length > 0){
                                aps.apsc.gotoView("user-new-1");
                            }
                            else{
                                messages.removeAll();
                                messages.addChild(new Message({description: _("You don't have enought resources to create a new user"), type: "error"}));
                                self.cancel();
                            }
                        });
                    });
                });
            });
        });
    });
