require([
    "aps/ResourceStore",
    "aps/Memory",
    "dojo/when",
    "dijit/registry",
    "aps/load",
    "dojox/mvc/getStateful",
    "dojox/mvc/getPlainValue",
    "aps/WizardData",
    "aps/Message",
    "dojox/mvc/at",
    "dojo/promise/all",
    "../common/TYPES.js",
    "aps/ready!"
], function (Store, Memory, when, registry, load, getStateful, getPlainValue, wizardData, Message, at, all, TYPES) {
    var store = new Store({
        apsType:        "http://aps-standard.org/types/core/service-user/1.0",
        target:        "/aps/2/resources"
    });
    var store2 = new Store({
        apsType:        TYPES.USER_TYPE,
        target:        "/aps/2/resources"
    });
    var wizards = [
        { text: _("Select User") },
        { text: _("Confirm Settings") }
    ];
    function generatePassword() {
        var length = 8,
            charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }
    var userModel = getStateful({model:{aps:{id:""}, fullName: "", email: "", login: "", owncloudpassword: ""}}),
        profilesModel = getStateful({model2:{aps:{id:""}, name: "", quota:"" }}),
        endUsers = new Memory({idProperty: "aps.id"}),
        profiles = new Memory({idProperty: "aps.id"}),
        categoriesChart = [],
        seriesData = [];
    load(["aps/PageContainer", { id: "pageContainer" }, [
        ["aps/WizardControl", { id: "wizard", steps: wizards }],
        ["aps/Container", { id: "contentcreation" }, [
            ["aps/Container", [
                ["aps/FieldSet", [
                    ["aps/Select", { id: "username", name: 'User Select', store: endUsers, value: at(userModel.model.aps, 'id'), labelAttr: 'displayName', title: _('User') }],
                    ["aps/Select", { id: "profilesdiv", title: _('Profile'), name: 'Profile Select', store: profiles, value: at(profilesModel.model2.aps, 'id'), labelAttr: 'name' }]
                ]],
                ["aps/FieldSet", { title: _('User Info'), id: "userinfo" }, [
                    ["aps/Output", { label: _('Name'), value: at(userModel.model, 'fullName') }],
                    ["aps/Output", { label: _('Email'), value: at(userModel.model, 'email') }],
                    ["aps/Output", { label: _('Login'), value: at(userModel.model, 'login') }],
                    ["aps/Password", { label: _('Service Password'), value: at(userModel.model, 'owncloudpassword'), required: true, showStrengthIndicator: false, showResetButton: false }]
                ]]
            ]]
        ]]
    ]]).then(function(){
        var page = registry.byId("pageContainer"),
            messages = page.get("messageList");
        wizards[0].active = true;
        var storesubscription = new Store({
                apsType:        "http://aps-standard.org/types/core/subscription/1.0",
                target:        "/aps/2/resources/" + aps.context.vars.tenant.aps.id
            }),
            firstprofile;
        storesubscription.query().then(function(thesubscription){
            return (new Store({
                target: "/aps/2/resources/" + thesubscription.subscription.aps.id + "/resources"
            })).query();
        }).then(function(theresources){

            function apsIdChanged2(prop, oldVal, newVal) {
                if (oldVal == newVal) return;
                profilesModel.set("model2",profiles.get(newVal));
                profilesModel.model2.aps.watch("id", apsIdChanged2);
            }

            var count = 0;
            for (var i = 0; i < theresources.length; i++){
                if(theresources[i].apsType == TYPES.SAMPLES_TYPE){
                    if((theresources[i].usage < theresources[i].limit) || (theresources[i].limit === undefined)){
                        // OK this resource is suitable, but we don't know the name...let's query
                        var storesamplename = new Store({
                            target: "/aps/2/resources/" + theresources[i].apsId
                        });
                        when(storesamplename.query(),function(samplename){
                            count++;
                            if(!firstprofile) firstprofile=samplename;
                            profiles.add(samplename);
                            categoriesChart.push({aps:{id: samplename.aps.id}, name:samplename.name, quota:samplename.quota});
                            seriesData.push([samplename.name, samplename.quota]);
                            registry.byId("profilesdiv").set("store",profiles);
                            if (firstprofile) {
                                profilesModel.set("model2",firstprofile);
                                profilesModel.model2.aps.watch("id", apsIdChanged2);
                            }
                        });
                    } else {
                        count++;
                    }
                } else {
                    count++;
                }
            }
        });

        var firstUser;
        all([store2.query(), store.query()]).then(function(res){
            var theowncloudusers = res[0],
                theusers = res[1];
            for (var i = 0; i < theusers.length; i++) {
                var found = 0;
                for (var j=0; j < theowncloudusers.length; j++){
                    if(theowncloudusers[j].owncloudusername==theusers[i].login){
                        found = 1;
                    }
                }
                if(found === 0){
                    if (!firstUser) firstUser = theusers[i];
                    endUsers.add(theusers[i]);
                }
            }
            registry.byId("username").set("store", endUsers);

            function apsIdChanged(prop, oldVal, newVal) {
                if (oldVal == newVal) return;
                userModel.set("model", endUsers.get(newVal));
                userModel.model.set("owncloudpassword", generatePassword());
                userModel.model.aps.watch("id", apsIdChanged);
            }

            if (firstUser) {
                userModel.set("model", firstUser);
                userModel.model.set("owncloudpassword", generatePassword());
                userModel.model.aps.watch("id", apsIdChanged);
            } else {
                registry.byId("contentcreation").set('visible',false);
                registry.byId("wizard").set('visible',false);
                messages.removeAll();
                messages.addChild(new Message({description: _("There is no users to attach to OwnCloud, create user first from Users tab"), type:"warning"}));
            }
        });
    });
    aps.app.onNext = function() {
        wizardData.put({userModel: getPlainValue(userModel.model), profilesModel: getPlainValue(profilesModel.model2)});
        aps.apsc.gotoView("user-new-2");
    };
    aps.app.onCancel = function() {
        aps.apsc.gotoView("ccpusers");
    };
});
