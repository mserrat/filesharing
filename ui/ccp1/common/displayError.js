define(["dijit/registry", "aps/Message"],
	function (registry, Message) {
		return function(err) {
			var errData = {};
			/* Getting the error message */
			try {errData = eval("(" + err.response.text + ")");} catch (e){}
			aps.apsc.cancelProcessing();

			var messages = registry.byId("page").get("messageList");
			/* Remove all current messages from the screen */
			messages.removeAll();
			/* And display the new message */
			messages.addChild(new Message({description: err + (errData.message ?
					"<br />" + errData.message : ""), type: "error"}));
		};
	}
);
