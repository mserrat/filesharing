<?php
if( strlen($argv[1]) < 3 ){
	print "file name is too short, doesn't seam good to me";
	die;
}
$inifile = parse_ini_file('./config.ini');
foreach($inifile as $key => $value){
	print "Setting type to $value on file ".$argv[1]."\n";
	$remplace =  replace_in_file($argv[1], $key, $value);	
	print $remplace["message"]."\n";
}

function replace_in_file($FilePath, $OldText, $NewText)
{
    $Result = array('status' => 'error', 'message' => '');
    if(file_exists($FilePath)===TRUE)
    {
        if(is_writeable($FilePath))
        {
            try
            {
                $FileContent = file_get_contents($FilePath);
                $FileContent = str_replace($OldText, $NewText, $FileContent);
                if(file_put_contents($FilePath, $FileContent) > 0)
                {
                    $Result["message"] = 'success';    
                }
                else
                {
                   $Result["message"] = 'Error while writing file'; 
                }
            }
            catch(Exception $e)
            {
                $Result["message"] = 'Error : '.$e; 
            }
        }
        else
        {
            $Result["message"] = 'File '.$FilePath.' is not writable !';       
        }
    }
    else
    {
        $Result["message"] = 'File '.$FilePath.' does not exist !';
    }
    return $Result;
}
?>
