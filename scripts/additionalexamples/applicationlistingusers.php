<?php
define('APS_PHP_RUNTIME_ROOT', '/usr/share/aps/php/');
require_once 'aps/2/aps.php';

#############################################################################################################################################
## this code is just an example of using the runtime, what it does is following								    #
## as application, it connects to controller and retrives all instances of the application						    #
## foreach application instance, it lists all the resourcs of it									    #
## even that is retrived when listing the resources owned by application, application goes over each tenant to list the users that belongs  #
## to them																    #
#############################################################################################################################################

foreach(\APS\ControllerProxy::listInstances() as $instanceId) {
	$apsc = \APS\Request::getController($instanceId);
	$resList = $apsc->getResources();
	foreach($resList as $resource){
		if($resource->aps->type=="http://owncloud.org/tenant-ldap/1.1"){
			$resource2 = $apsc->getResource($resource->aps->id);
			$i=1;
			foreach($resource2->users as $user){
				print "user $i\n";
				print var_dump($apsc->getResource($user->link->id));
				$i++;
			}
		}
	}
}
?>
