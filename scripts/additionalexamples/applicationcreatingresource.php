<?php
define('APS_PHP_RUNTIME_ROOT', '/usr/share/aps/php/');
require_once 'aps/2/aps.php';
require_once '../samples.php';

#############################################################################################################################################
## This script is just a sample of a feature supported by runtime, let's imagine than application as owner of it's own types wants to create#
## a resource for itself, either on a concrete subsription, either as the application instance						    #
## in Below code, what is being done is create a resource (concretely a sample but as application, in POA nothing is done, no user          #
## intervention, just application accessing and creating such a test sample with certain values on the properties                           #
## In order to get more information about registering resources, please visit the APS 2.0 specification website, concretely		    #
##  http://debug.dev.aps.sw.ru/doc/spec/controller-api/applications.html#register-resource 						    #
#############################################################################################################################################
	
foreach(\APS\ControllerProxy::listInstances() as $instanceId) {
	$apsc = \APS\Request::getController($instanceId);
	$resource= new samples();

	$resource->aps = new \APS\ResourceMeta("http://owncloud.org/samples/1.1");
	$resource->quota=10;
	$resource->name="TES2T";
	$output = $apsc->registerResource($resource);
	print var_dump($output);
}
?>
