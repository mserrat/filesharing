<?php
        # This simple examples is thought to run over CLI on app server, goal is to get all admin users of all accounts who has owncloud
        # script takes into consideration that maybe exists multiple instances pointing to same endpoint
        define('APS_PHP_RUNTIME_ROOT', '/usr/share/aps/php/');
        require_once 'aps/2/aps.php';
        foreach(\APS\ControllerProxy::listInstances() as $instanceId) {
                $apsc = \APS\Request::getController($instanceId);
                $resList = $apsc->getResources("implementing(http://owncloud.org/tenant-ldap/1.0)");
                ##Let's print admin email for each account
                foreach($resList as $resource){
                        ## We are console script, and to be able to act as customer, what we can do is impersonate to a concrete resource
                         \APS\Request::getController()->setResourceId($resource->aps->id);
                        #We will retrive subscription in order to show that is possible, even that for this example is not needed
                        $subscription=$apsc->getResources("implementing(http://parallels.com/aps/types/pa/subscription/1.0)");
                        print "I'm working with subscription ".$subscription[0]->subscriptionId."\n";
                        ###let's get the subscription!
                        $theconcretesubscription=$apsc->getResource($subscription[0]->aps->id);
                        ###let's do multi itineration, is clear that we can instead of going over subscription, ask directly who owns such resource but i think is interesting to see how to follow links
                        $account=$apsc->getResource($theconcretesubscription->account->aps->id);
                        print "the susbcription ".$theconcretesubscription->subscriptionId." is owned by company ".$account->companyName."\n";
                        #And now...let's see who is the admin! :)
                        $admins = $apsc->getResources("implementing(http://parallels.com/aps/types/pa/admin-user/1.0)", "/aps/2/resources/".$account->aps->id."/users");
                        print "Our admin email is: ".$admins[0]->email;
                        ###return impersonation back
                        \APS\Request::getController()->setResourceId(null);
                }
        }
?>
